﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Routing;

namespace Router
{
    class Program
    {
        static void Main(string[] args)
        {

            var routeTo = "http://localhost:64717/Calculator.svc";
            var routerAddr = "http://localhost:8899/Router";
            var h = new ServiceHost(typeof(RoutingService));
            h.AddServiceEndpoint(typeof(IRequestReplyRouter),
            new BasicHttpBinding(), routerAddr);
            var rc = new RoutingConfiguration();
            var contract = ContractDescription.GetContract(typeof(IRequestReplyRouter));
            var client = new ServiceEndpoint(contract, new BasicHttpBinding(),
            new EndpointAddress(routeTo));
            var lst = new List<ServiceEndpoint>();
            lst.Add(client);
            rc.FilterTable.Add(new MatchAllMessageFilter(), lst);
            h.Description.Behaviors.Add(new RoutingBehavior(rc));
            h.Open();
            Console.WriteLine("Opened");
            Console.ReadKey();
        }
    }
}
