﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    [ServiceContract]
    interface ICalculator
    {
        [OperationContract]
        [WebGet(UriTemplate = "dodaj/{a}/{b}", ResponseFormat = WebMessageFormat.Json)]
        int Add(string a, string b);

        [OperationContract]
        [WebInvoke(UriTemplate = "testForm")]
        int Form(System.IO.Stream s);
    }

    public class Calculator : ICalculator
    {
        public int Add(string a, string b)
        {
            return Int32.Parse(a) + Int32.Parse(b);
        }

        public int Form(Stream s)
        {
            byte[] d = new byte[150];
            s.Read(d, 0, 150);
            string s2 = ASCIIEncoding.ASCII.GetString(d);
            Console.WriteLine("zawartosc formularza = {0}", s2);
            return 42;
        }
    }
}
