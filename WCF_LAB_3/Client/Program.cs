﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    [ServiceContract]
    interface ICalculator
    {
        [OperationContract]
        [WebGet(UriTemplate = "dodaj/{a}/{b}", ResponseFormat = WebMessageFormat.Json)]
        int Add(string a, string b);

        [OperationContract]
        [WebInvoke(UriTemplate = "testForm")]
        int Form(System.IO.Stream s);
    }

    class Program
    {
        static void Main(string[] args)
        {
            //var f = new ChannelFactory<ICalculator>(new WebHttpBinding(),
            //    new EndpointAddress("http://localhost:64717/Calculator.svc"));

            var f = new ChannelFactory<ICalculator>(new WebHttpBinding(),
                new EndpointAddress("http://localhost:8899/Router"));


            f.Endpoint.Behaviors.Add(new WebHttpBehavior());

            var c = f.CreateChannel();

            Console.WriteLine(c.Add("3", "55"));

            ((IDisposable)c).Dispose();
            Console.ReadKey();
        }
    }
}
