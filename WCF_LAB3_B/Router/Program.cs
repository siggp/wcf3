﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Routing;


namespace Router
{
    class Program
    {
        static void Main(string[] args)
        {

            string clientAddress = "http://localhost/usluga";
            string routerAddress = "http://localhost/Router";

            var routerBinding = new WSHttpBinding();
            var clientBinding = new WSHttpBinding();

            var serviceHost = new ServiceHost(typeof(RoutingService));
            //add the endpoint the router will use to recieve messages
            serviceHost.AddServiceEndpoint(typeof(IRequestReplyRouter), routerBinding, routerAddress);


            //create the client endpoint the router will route messages to
            ContractDescription contract = ContractDescription.GetContract(typeof(IRequestReplyRouter));
            ServiceEndpoint client = new ServiceEndpoint(contract, clientBinding, new EndpointAddress(clientAddress));

            //create a new routing configuration object
            RoutingConfiguration rc = new RoutingConfiguration();

            //create the endpoint list that contains the service endpoints we want to route to
            //in this case we have only one
            List<ServiceEndpoint> endpointList = new List<ServiceEndpoint>();
            endpointList.Add(client);

            //add a MatchAll filter to the Router's filter table
            //map it to the endpoint list defined earlier
            //when a message matches this filter, it will be sent to the endpoint contained in the list
            rc.FilterTable.Add(new MatchAllMessageFilter(), endpointList);

            //attach the behavior to the service host
            serviceHost.Description.Behaviors.Add(new RoutingBehavior(rc));



            serviceHost.Open();


            //var routeTo = "http://localhost/usluga";
            //var routerAddr = "http://localhost/Router";
            //var h = new ServiceHost(typeof(RoutingService));
            //h.AddServiceEndpoint(
            //    typeof(IRequestReplyRouter),
            //    new WSHttpBinding(),
            //    routerAddr
            //);

            //var rc = new RoutingConfiguration();
            //var contract = ContractDescription.GetContract(typeof(IRequestReplyRouter));
            //var client = new ServiceEndpoint(contract, new WSHttpBinding(),
            //new EndpointAddress(routeTo));
            //var lst = new List<ServiceEndpoint>();
            //lst.Add(client);
            //rc.FilterTable.Add(new MatchAllMessageFilter(), lst);
            //h.Description.Behaviors.Add(new RoutingBehavior(rc));


            //h.Open();
            Console.ReadKey();
            serviceHost.Close();
        }
    }
}
