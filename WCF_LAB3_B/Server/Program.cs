﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    [ServiceContract]
    interface ICalculator
    {
        [OperationContract]
        [WebGet(UriTemplate = "dodaj/{a}/{b}", ResponseFormat = WebMessageFormat.Json)]
        int Add(string a, string b);
    }

    public class Calculator : ICalculator
    {
        public int Add(string a, string b)
        {
            return Int32.Parse(a) + Int32.Parse(b);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            var host = new ServiceHost(typeof(Calculator), new Uri("http://localhost"));
            var ep = host.AddServiceEndpoint(
                typeof(ICalculator),
                new WebHttpBinding(),
                "Usluga"
            );

            ep.Behaviors.Add(new WebHttpBehavior());

            host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
            host.AddServiceEndpoint(new UdpDiscoveryEndpoint("soap.udp://localhost:25551"));

            host.Open();
            Console.ReadKey();
            host.Close();
        }
    }
}
