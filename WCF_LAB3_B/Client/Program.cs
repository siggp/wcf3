﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    [ServiceContract]
    interface ICalculator
    {
        [OperationContract]
        [WebGet(UriTemplate = "dodaj/{a}/{b}", ResponseFormat = WebMessageFormat.Json)]
        int Add(string a, string b);
    }

    class Program
    {
        static void Main(string[] args)
        {
            var sx = 5;
            DiscoveryClient discoveryClient =
                new DiscoveryClient(new UdpDiscoveryEndpoint("soap.udp://localhost:25551"));
            System.Collections.ObjectModel.Collection<EndpointDiscoveryMetadata> lst =
            discoveryClient.Find(new FindCriteria(typeof(ICalculator))).Endpoints;
            discoveryClient.Close();
            if (lst.Count > 0)
            {
                var addr = lst[0].Address; // łączymy się z pierwszym znalezionym

                var f = new ChannelFactory<ICalculator>(new WebHttpBinding(),
                    new EndpointAddress(addr.ToString()));

                f.Endpoint.Behaviors.Add(new WebHttpBehavior());

                var c = f.CreateChannel();
                Console.WriteLine(c.Add("3", "5"));

                Console.ReadKey();
            }
        }
    }
}
